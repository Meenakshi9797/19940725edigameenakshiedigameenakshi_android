package com.amel.a19940725_edigameenakshi_edigameenakshi.Model;

import com.google.gson.annotations.SerializedName;

public class SchoolsListModel {

    @SerializedName("dbn")
    String dbn;

    @SerializedName("school_name")
    String school_name;

    @SerializedName("overview_paragraph")
    String overview_paragraph;

    @SerializedName("location")
    String location;

    @SerializedName("phone_number")
    String phone_number;

    @SerializedName("school_email")
    String school_email;

    @SerializedName("website")
    String website;

    @SerializedName("total_students")
    String total_students;

    @SerializedName("extracurricular_activities")
    String extracurricular_activities;

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getOverview_paragraph() {
        return overview_paragraph;
    }

    public void setOverview_paragraph(String overview_paragraph) {
        this.overview_paragraph = overview_paragraph;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getSchool_email() {
        return school_email;
    }

    public void setSchool_email(String school_email) {
        this.school_email = school_email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getTotal_students() {
        return total_students;
    }

    public void setTotal_students(String total_students) {
        this.total_students = total_students;
    }

    public String getExtracurricular_activities() {
        return extracurricular_activities;
    }

    public void setExtracurricular_activities(String extracurricular_activities) {
        this.extracurricular_activities = extracurricular_activities;
    }
}

