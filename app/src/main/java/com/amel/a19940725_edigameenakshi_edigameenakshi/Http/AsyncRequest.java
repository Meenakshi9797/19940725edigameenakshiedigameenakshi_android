package com.amel.a19940725_edigameenakshi_edigameenakshi.Http;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.View;
import android.widget.LinearLayout;

import com.amel.a19940725_edigameenakshi_edigameenakshi.R;
import com.amel.a19940725_edigameenakshi_edigameenakshi.Utils.GeneralUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AsyncRequest extends AsyncTask<String, Integer, String> {

    OnAsyncRequestComplete caller;
    Activity context;
    String method = "GET";
    JSONObject postJsonObject;
    ProgressDialog pDialog = null;
    String response = "";
    String boomerangURL;
    String flagTypeValue;

    // Three Constructors
    public AsyncRequest(OnAsyncRequestComplete requestComplete, Activity activity, String method, String url, JSONObject jsonObject, String flagType) {
        caller = requestComplete;
        this.context = activity;
        this.method = method;
        this.boomerangURL = url;
        this.postJsonObject = jsonObject;
        this.flagTypeValue = flagType;
    }

    /*** GET CALL Constructor ***/
    public AsyncRequest(OnAsyncRequestComplete requestComplete, Activity activity, String method, String url, String flagType) {
        //caller = (OnAsyncRequestComplete) activity;
        this.caller = requestComplete;
        this.context = activity;
        this.method = method;
        this.boomerangURL = url;
        this.flagTypeValue = flagType;
    }

    public AsyncRequest(Activity activity, String url,String flagType) {
        caller = (OnAsyncRequestComplete) activity;
        this.context = activity;
        this.boomerangURL = url;
        this.flagTypeValue = flagType;
    }

    // Interface to be implemented by calling activity
    public interface OnAsyncRequestComplete {
        void asyncResponse(String response, String flagType);
        void refreshData();
    }

    @Override
    public String doInBackground(String... urls) {
        // get url pointing to entry point of API

        //String address = urls[0].toString();
        if (method == "POST") {
            try {
                System.out.println("POST_URL = "+boomerangURL);
                response = ServiceHandler.processPostRequest(boomerangURL,postJsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        if (method == "GET") {
            System.out.println("GET_URL = "+boomerangURL);
            response = ServiceHandler.processGetRequest(boomerangURL,30000);
            return response;
        }

        int max = 1024;
        for (int i = 0; i < max; i++) {
            int percentageCompleted = i*100/max;
            publishProgress(percentageCompleted);
        }

        return "";
    }

    @Override
    public void onPreExecute() {
        pDialog = ProgressDialog.show(context, "", "", true);
        pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pDialog.setContentView(R.layout.progress_dialog);
        pDialog.setCancelable(false);
        setProgressView(true,"");

    }

    private void setProgressView(boolean isProgressView,String response) {
        if(isProgressView){
        }else{
            if(response != null && response.length() >0) {
                GeneralUtil.showAlert(context, response);
                dismissDialog();
            }
        }
    }

    @Override
    public void onProgressUpdate(Integer... progress) {
        // you can implement some progressBar and update it in this record
        //textVIewMessageTitle.setText("Loading data ("+progress[0]+").");
    }

    @Override
    public void onPostExecute(String response) {

        if(response != null && response.length() > 0 && isJson(response)){
            caller.asyncResponse(response,flagTypeValue);
            dismissDialog();
        }else{
            if(pDialog != null && pDialog.isShowing()){
                setProgressView(false,response);
                dismissDialog();
            }
        }
    }

    @Override
    protected void onCancelled(String response) {
        dismissDialog();
    }

    public boolean isJson(String Json) {
        if(Json != null && Json.length() > 0){
            try {
                new JSONObject(Json);
            } catch (JSONException ex) {
                try {
                    new JSONArray(Json);
                } catch (JSONException ex1) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public void dismissDialog(){
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}