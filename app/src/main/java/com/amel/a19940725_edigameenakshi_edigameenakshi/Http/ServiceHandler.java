package com.amel.a19940725_edigameenakshi_edigameenakshi.Http;


import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;


public class ServiceHandler {

    /**
     * Generic method to execute all the IO operations, helps to connect to
     * given url and get response.
     *
     * @param url url to be connected.
     * @return Raw response from server String
     */
    public static String processGetRequest(String url, int timeout){

        HttpURLConnection urlConnection = null;
        String results = null;
        try
        {
            URL urlCopy = new URL(url); //Assuming the given path, this can be anything
            urlConnection =  (HttpURLConnection) urlCopy.openConnection();  //Open the connection with the DB

            /*** Online Authorization **/
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setUseCaches(false);
            urlConnection.setAllowUserInteraction(false);
            urlConnection.setConnectTimeout(timeout);
            urlConnection.setReadTimeout(timeout);
            urlConnection.connect();
            int status = urlConnection.getResponseCode();

            switch (status){

                case 201:
                case 200:
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null)
                    {
                        sb.append(line+"\n");
                    }
                    br.close();
                    Log.e("ServiceResponse= ", sb.toString());
                    return sb.toString();

            }
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                try {
                    urlConnection.disconnect();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return "";
    }



    public static String processPostRequest(String postCallUrl, JSONObject jsonParam) throws Exception {

        String contentAsString = "";
        InputStream inputStream;
        URL url = new URL(postCallUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        /*** Online Authorization **/
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setDoInput(true);
        //connection.setInstanceFollowRedirects(false);
        connection.setRequestProperty("Content-Type", "application/json");
        //connection.setRequestProperty("Accept", "application/json");
        //connection.setRequestProperty("charset", "utf-8");
        //connection.setRequestProperty("Authorization", "Basic");
        connection.setConnectTimeout(45000);
        connection.setReadTimeout(45000);
        connection.setUseCaches(false);
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
        System.out.println("ServiceResponse-->postCallUrl="+postCallUrl);
        System.out.println("ServiceResponse-->Sending Json Params="+jsonParam.toString());
        wr.writeBytes(jsonParam.toString());
        wr.flush();
        wr.close();

        //do somehting with response
        try {
            int responseCode = connection.getResponseCode();
            if(responseCode >= HttpURLConnection.HTTP_BAD_REQUEST){
                inputStream = connection.getErrorStream();
                contentAsString = readStream(inputStream);
                inputStream.close();
                System.out.println("ServiceResponse=" + contentAsString + "---" + responseCode);
            }else{
                inputStream = connection.getInputStream();
                contentAsString = readStream(inputStream);
                inputStream.close();
                System.out.println("ServiceResponse=" + contentAsString + "---" + responseCode);
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return "SocketTimeoutException";
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (connection != null) {
            connection.disconnect();
        }

        return contentAsString;

    }

    private static String readStream(InputStream inputStream) throws IOException {
        if (inputStream == null) {
            return null;
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();

        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }
        return sb.toString();

    }
}
