package com.amel.a19940725_edigameenakshi_edigameenakshi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.amel.a19940725_edigameenakshi_edigameenakshi.Adapter.SchoolsListAdapter;
import com.amel.a19940725_edigameenakshi_edigameenakshi.Http.API;
import com.amel.a19940725_edigameenakshi_edigameenakshi.Http.AsyncRequest;
import com.amel.a19940725_edigameenakshi_edigameenakshi.Model.SchoolsListModel;
import com.amel.a19940725_edigameenakshi_edigameenakshi.Utils.GeneralUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AsyncRequest.OnAsyncRequestComplete {

    RecyclerView rRecyclerView;
    public static MainActivity mainActivity;
    public static MainActivity getMainActivity(){
        if(mainActivity == null){
            return new MainActivity();
        }
        return mainActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rRecyclerView = (RecyclerView) findViewById(R.id.rRecyclerView);
        mainActivity = this;
        getListOfSchools();
    }


    private void getListOfSchools() {
        try {
            boolean checkInternetConnection = GeneralUtil.isOnline(getApplicationContext());
            if (checkInternetConnection) {
                AsyncRequest asyncRequest = new AsyncRequest(MainActivity.this, MainActivity.this, "GET", API.GET_SCHOOLS_LIST, "GET_SCHOOLS_LIST");
                asyncRequest.execute();
            } else {
                GeneralUtil.showToast(mainActivity,"No Internet connection");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void asyncResponse(String response, String flagType) {
        switch (flagType) {
            case "GET_SCHOOLS_LIST":
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if(jsonArray.length() > 0){

                        Gson gson = new Gson();
                        Type collectionType = new TypeToken<List<SchoolsListModel>>(){}.getType();
                        ArrayList<SchoolsListModel> userSites  = gson.fromJson( response , collectionType);
                        if(userSites != null && userSites.size() > 0){
                            setSchoolsListAdapter(userSites);
                        }

                    }else{
                        GeneralUtil.showToast(mainActivity,"No Response");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void setSchoolsListAdapter(ArrayList<SchoolsListModel> arrayList) {
        SchoolsListAdapter adapter = new SchoolsListAdapter(mainActivity, arrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mainActivity);
        rRecyclerView.setLayoutManager(mLayoutManager);
        rRecyclerView.setItemAnimator(new DefaultItemAnimator());
        rRecyclerView.setAdapter(adapter);
    }

    @Override
    public void refreshData() {

    }
}