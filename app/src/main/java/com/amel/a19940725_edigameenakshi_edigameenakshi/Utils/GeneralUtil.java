package com.amel.a19940725_edigameenakshi_edigameenakshi.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.Html;
import android.widget.Toast;

import com.amel.a19940725_edigameenakshi_edigameenakshi.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class GeneralUtil {

    public static boolean isJson(String Json) {
        if (Json != null && Json.length() > 0) {
            try {
                new JSONObject(Json);
            } catch (JSONException ex) {
                try {
                    new JSONArray(Json);
                } catch (JSONException ex1) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }


    public static void showNetworkAlertWithFinish(final Activity activity) {
        new AlertDialog.Builder(activity)
                .setTitle("No Internet Connection!")
                .setMessage("Sorry, No Internet connectivity detected.\nPlease reconnect and try again.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        activity.finish();
                    }
                }).show();
    }

    public static void showAlert(Activity activity, String message) {
        new AlertDialog.Builder(activity)
                .setMessage(Html.fromHtml(message))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                }).show();

    }

    public static boolean isOnline(Context context) {
        if (context != null) {
            try {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                return netInfo != null && netInfo.isConnected();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static void showToast(Activity mainActivity,String msg) {
        Toast.makeText(mainActivity,msg,Toast.LENGTH_LONG).show();
    }
}
