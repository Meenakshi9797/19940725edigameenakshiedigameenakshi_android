package com.amel.a19940725_edigameenakshi_edigameenakshi;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.amel.a19940725_edigameenakshi_edigameenakshi.Http.API;
import com.amel.a19940725_edigameenakshi_edigameenakshi.Http.AsyncRequest;
import com.amel.a19940725_edigameenakshi_edigameenakshi.Model.SchoolsInfoModel;
import com.amel.a19940725_edigameenakshi_edigameenakshi.Model.SchoolsListModel;
import com.amel.a19940725_edigameenakshi_edigameenakshi.Utils.GeneralUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SchoolInfoActivity extends AppCompatActivity implements AsyncRequest.OnAsyncRequestComplete {

    TextView txt_school_name,txt_num_of_sat_test_takers,
            txt_sat_critical_reading_avg_score,txt_sat_writing_avg_score,
            txt_sat_math_avg_score,txt_No_Info;
    LinearLayout LL_InfoLayout;
    String argDBN = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_info);

        txt_school_name                    = findViewById(R.id.txt_school_name);
        txt_num_of_sat_test_takers         = findViewById(R.id.txt_num_of_sat_test_takers);
        txt_sat_critical_reading_avg_score = findViewById(R.id.txt_sat_critical_reading_avg_score);
        txt_sat_writing_avg_score          = findViewById(R.id.txt_sat_writing_avg_score);
        txt_sat_math_avg_score             = findViewById(R.id.txt_sat_math_avg_score);
        txt_No_Info                        = findViewById(R.id.txt_No_Info);
        LL_InfoLayout                      = (LinearLayout) findViewById(R.id.LL_InfoLayout);

        argDBN = getIntent().getStringExtra("DBN");
        getSchoolInfoAPI();

    }

    private void getSchoolInfoAPI() {
        try {
            boolean checkInternetConnection = GeneralUtil.isOnline(getApplicationContext());
            if (checkInternetConnection) {
                AsyncRequest asyncRequest = new AsyncRequest(SchoolInfoActivity.this, SchoolInfoActivity.this,
                        "GET", API.GET_SCHOOLS_INFO+argDBN, "GET_SCHOOL_INFO");
                asyncRequest.execute();
            } else {
                GeneralUtil.showToast(SchoolInfoActivity.this,"No Internet connection");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void asyncResponse(String response, String flagType) {
        switch (flagType) {
            case "GET_SCHOOL_INFO":
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if(jsonArray.length() > 0){
                        Gson gson = new Gson();
                        Type collectionType = new TypeToken<List<SchoolsInfoModel>>(){}.getType();
                        ArrayList<SchoolsInfoModel> userSites  = gson.fromJson( response , collectionType);
                        if(userSites != null && userSites.size() > 0){
                            txt_school_name.setText(userSites.get(0).getSchool_name());
                            txt_num_of_sat_test_takers.setText(userSites.get(0).getNum_of_sat_test_takers());
                            txt_sat_critical_reading_avg_score.setText(userSites.get(0).getSat_critical_reading_avg_score());
                            txt_sat_writing_avg_score.setText(userSites.get(0).getSat_writing_avg_score());
                            txt_sat_math_avg_score.setText(userSites.get(0).getSat_math_avg_score());
                        }else{
                            LL_InfoLayout.setVisibility(View.GONE);
                            txt_No_Info.setVisibility(View.VISIBLE);
                        }

                    }else{
                        LL_InfoLayout.setVisibility(View.GONE);
                        txt_No_Info.setVisibility(View.VISIBLE);
                        GeneralUtil.showToast(SchoolInfoActivity.this,"No Response");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void refreshData() {

    }
}
