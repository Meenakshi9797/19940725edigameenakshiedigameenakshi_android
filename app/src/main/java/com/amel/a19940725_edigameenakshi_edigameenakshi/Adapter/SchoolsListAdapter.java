package com.amel.a19940725_edigameenakshi_edigameenakshi.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.amel.a19940725_edigameenakshi_edigameenakshi.Model.SchoolsListModel;
import com.amel.a19940725_edigameenakshi_edigameenakshi.R;
import com.amel.a19940725_edigameenakshi_edigameenakshi.SchoolInfoActivity;

import java.util.ArrayList;

public class SchoolsListAdapter extends RecyclerView.Adapter<SchoolsListAdapter.ViewHolder> {

    Activity activity;
    ArrayList<SchoolsListModel> arrayListTemplates;

    public SchoolsListAdapter(FragmentActivity activity, ArrayList<SchoolsListModel> arrayList) {
        this.arrayListTemplates = arrayList;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.layout_schools_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final SchoolsListModel model = arrayListTemplates.get(position);
        holder.txt_school_name.setText(model.getSchool_name());
        holder.txt_overview_paragraph.setText(model.getOverview_paragraph());
        holder.txt_location.setText(model.getLocation());
        holder.txt_phone_number.setText(model.getPhone_number());
        holder.txt_website.setText(model.getWebsite());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentSchoolInfo = new Intent(activity, SchoolInfoActivity.class);
                intentSchoolInfo.putExtra("DBN",model.getDbn());
                activity.startActivity(intentSchoolInfo);
            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayListTemplates.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_school_name;
        TextView txt_overview_paragraph;
        TextView txt_location;
        TextView txt_phone_number;
        TextView txt_website;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_school_name = (TextView) itemView.findViewById(R.id.txt_school_name);
            txt_overview_paragraph = (TextView) itemView.findViewById(R.id.txt_overview_paragraph);
            txt_location = (TextView) itemView.findViewById(R.id.txt_location);
            txt_phone_number = (TextView) itemView.findViewById(R.id.txt_phone_number);
            txt_website = (TextView) itemView.findViewById(R.id.txt_website);
        }
    }
}
